
#ifndef _MOON_H_
#define _MOON_H_ _MOON_H_

#include <png.h>
#include "image.h"

struct moon_s
{
   png_bytep    bitmap;
   unsigned int width;
   unsigned int height;
   unsigned int xbytes;
};

typedef struct moon_s moon_t;

moon_t *mooncreate();
void moondestroy( moon_t *moon );
void mooncopy( image_t *image, moon_t *moondata, int x, int y, int blackflag );

#endif
