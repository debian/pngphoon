
CC=gcc
CFLAGS += -Os -Wall -Wextra -pedantic -DSYS5=1
LDFLAGS += -lpng -lz -lm

all: pngphoon

pngphoon: image.o pngwrite.o moon.o main.o phase.o tws.o stars.o fail.o
	$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean:
	rm -f *.o pngphoon

